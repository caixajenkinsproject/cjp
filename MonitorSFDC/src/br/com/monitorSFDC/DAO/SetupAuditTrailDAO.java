package br.com.monitorSFDC.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.sobject.SetupAuditTrail;
import com.sforce.ws.ConnectionException;

public class SetupAuditTrailDAO
{
	public static QueryResult getTrailInformation(EnterpriseConnection connection) throws ConnectionException
	{
		String soqlQuery = "SELECT Id,Action,CreatedBy.Name,CreatedDate,Display,Section FROM SetupAuditTrail WHERE CreatedDate = LAST_N_DAYS:4";
		QueryResult result = connection.query(soqlQuery);
		if(result.getSize() > 0)
		{
			return result;
		}else
		{
			throw new ConnectionException("No records found.");
		}
	}
	
	public static void insertTrailInformation(SetupAuditTrail setupAudit,Connection connection) throws SQLException
	{
		String sqlInsert = "INSERT INTO "
				+ "monitoramentosfdc.sfdc_changes"
				+ "("
					+ "sfdc_changes_id_log,"
					+ "sfdc_changes_display,"
					+ "sfdc_changes_section,"
					+ "sfdc_changes_action,"
					+ "sfdc_changes_user,"
					+ "sfdc_changes_createdDate)"
				+ "VALUES(?,?,?,?,?,?);";
		PreparedStatement statement = connection.prepareStatement(sqlInsert);
		
		statement.setString(1, setupAudit.getId());
		statement.setString(2, setupAudit.getDisplay());
		statement.setString(3, setupAudit.getSection());
		statement.setString(4, setupAudit.getAction());
		statement.setString(5, setupAudit.getCreatedBy().getName());
		
		Date createdDate = new Date(setupAudit.getCreatedDate().getTimeInMillis());
		statement.setDate(6, createdDate);
		
		statement.execute();
	}
}
