package br.com.monitorSFDC.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BD
{
	private static final String serverName = "localhost";
	private static final String myDatabase = "monitoramentosfdc";
	private static final String url = "jdbc:mysql://"+ serverName + "/"+ myDatabase;
	private static final String username = "root";
	private static final String password = "dtt.2017";
	
	public static Connection connection;

	public static Connection getConnection()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url, username, password);
			return connection;
		} catch (ClassNotFoundException | SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean fecharConexao()
	{
		try
		{
			BD.getConnection().close();
			return true;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}

}
