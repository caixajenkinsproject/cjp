package br.com.monitorSFDC.DAO;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.ws.ConnectionException;

public class ApexClassDAO
{
	public static QueryResult getApexClassWithoutTestAndMock(EnterpriseConnection connection) throws ConnectionException
	{
		String soqlQuery = "Select Status, Name,CreatedBy.Name, CreatedDate, LastModifiedBy.Name,LastModifiedDate From ApexClass "
				+ "WHERE (NOT Name LIKE \'%Test%\') "
				+ "AND (NOT Name LIKE \'%Mock%\')";
		
		QueryResult result = connection.query(soqlQuery);
		if(result.getSize() > 0)
		{
			return result;
		}else
		{
			throw new ConnectionException("No records found.");
		}
	}
}
