package br.com.monitorSFDC.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.sforce.soap.tooling.QueryResult;
import com.sforce.soap.tooling.ToolingConnection;
import com.sforce.soap.tooling.sobject.ApexCodeCoverage;
import com.sforce.soap.tooling.sobject.ApexCodeCoverageAggregate;
import com.sforce.ws.ConnectionException;

public class ApexCodeCoverageAggregateDAO
{
	public static QueryResult getApexCodeCoverage(ToolingConnection connection) throws ConnectionException
	{
		String soqlQuery = "SELECT id,ApexClassorTrigger.Name,NumLinesCovered,NumLinesUncovered FROM ApexCodeCoverageAggregate";
		QueryResult result = connection.query(soqlQuery);
		if(result.getSize() > 0)
		{
			return result;
		}else
		{
			throw new ConnectionException("No records Found");
		}
	}
	
	public static void insertApexCodeCoverage(ApexCodeCoverageAggregate apexCodeCoverageAggregate,Connection connectionBD) throws SQLException
	{
		String sqlInsert = "INSERT INTO sfdc_coverage_test"
							+ "("
								+ "sfdc_coverage_test_name,"
								+ "sfdc_coverage_test_num_lines_coveredl,"
								+ "sfdc_coverage_test_num_lines_uncovered,"
								+ "sfdc_coverage_test_percent"
							+ ")VALUES(?,?,?,?);";
		
		Double linhasCobertas = (double)apexCodeCoverageAggregate.getNumLinesCovered();
		Double totalLines = (double) (apexCodeCoverageAggregate.getNumLinesCovered() + apexCodeCoverageAggregate.getNumLinesUncovered());
		
		Integer percent = 0;
		if(totalLines > 0)
		{
			percent = (int) Math.round((double) (linhasCobertas/totalLines)*100);
		}
				
		PreparedStatement statement = connectionBD.prepareStatement(sqlInsert);
		statement.setString(1, apexCodeCoverageAggregate.getApexClassOrTrigger().getName());
		statement.setInt(2, apexCodeCoverageAggregate.getNumLinesCovered());
		statement.setInt(3, apexCodeCoverageAggregate.getNumLinesUncovered());
		statement.setString(4, percent+"%");
		
		statement.execute();
	}
}
