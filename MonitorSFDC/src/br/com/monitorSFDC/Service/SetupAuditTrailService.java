package br.com.monitorSFDC.Service;

import java.sql.Connection;
import java.sql.SQLException;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.sobject.SObject;
import com.sforce.soap.enterprise.sobject.SetupAuditTrail;
import com.sforce.ws.ConnectionException;

import br.com.monitorSFDC.DAO.SetupAuditTrailDAO;

public class SetupAuditTrailService
{
	public static void consultAudit(EnterpriseConnection connection, Connection connectionDB) throws ConnectionException, SQLException
	{
		QueryResult result = SetupAuditTrailDAO.getTrailInformation(connection);
		boolean done = false;
		while (!done)
		{
			SObject[] records = result.getRecords();
			for (int i = 0; i < records.length; i++)
			{
				SetupAuditTrail setupAuditTrail = (SetupAuditTrail) records[i];
				System.out.println("getId = " + setupAuditTrail.getId());
				System.out.println("getDisplay = "+setupAuditTrail.getDisplay());
				System.out.println("getSection = "+setupAuditTrail.getSection());
				System.out.println("getAction = "+setupAuditTrail.getAction());
				System.out.println("Usuário = "+setupAuditTrail.getCreatedBy().getName());
				System.out.println("Usuário = "+setupAuditTrail.getCreatedDate().getTime().toString());
				System.out.println("\n\n\n");
				
				SetupAuditTrailDAO.insertTrailInformation(setupAuditTrail,connectionDB);
				if (result.isDone())
				{
					done = true;
				}else
				{
					result = connection.queryMore(result.getQueryLocator());
				}

			}
		}
	}
}
