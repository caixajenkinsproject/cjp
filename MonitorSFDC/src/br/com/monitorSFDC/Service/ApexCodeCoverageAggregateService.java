package br.com.monitorSFDC.Service;

import java.sql.Connection;
import java.sql.SQLException;

import com.sforce.soap.tooling.QueryResult;
import com.sforce.soap.tooling.ToolingConnection;
import com.sforce.soap.tooling.sobject.ApexCodeCoverage;
import com.sforce.soap.tooling.sobject.ApexCodeCoverageAggregate;
import com.sforce.soap.tooling.sobject.SObject;
import com.sforce.ws.ConnectionException;

import br.com.monitorSFDC.DAO.ApexCodeCoverageAggregateDAO;

public class ApexCodeCoverageAggregateService
{
	public static void consultApexCodeCoverage(ToolingConnection toolingConnection, Connection connectionDB) throws ConnectionException, SQLException
	{
		QueryResult result = ApexCodeCoverageAggregateDAO.getApexCodeCoverage(toolingConnection);
		boolean done = false;
		
		while(!done)
		{
			SObject[] records = result.getRecords();
			for (int i = 0; i < records.length; i++)
			{
				ApexCodeCoverageAggregate apexCodeCoverageAggregate = (ApexCodeCoverageAggregate) records[i];
				System.out.println("getId = "+apexCodeCoverageAggregate.getId());
				System.out.println("getName = "+apexCodeCoverageAggregate.getApexClassOrTrigger().getName());
				System.out.println("getNumLinesCovered = "+apexCodeCoverageAggregate.getNumLinesCovered());
				System.out.println("getNumLinesUncovered = "+apexCodeCoverageAggregate.getNumLinesUncovered());
				Double linhasCobertas = (double)apexCodeCoverageAggregate.getNumLinesCovered();
				Double totalLines = (double) (apexCodeCoverageAggregate.getNumLinesCovered() + apexCodeCoverageAggregate.getNumLinesUncovered());
				System.out.println("getTotalLines = "+totalLines);
				
				Integer percent = 0;
				if(totalLines > 0)
				{
					percent = (int) Math.round((double) (linhasCobertas/totalLines)*100);
				}
				
				System.out.println("percent = "+percent+"%");
				System.out.println("\n");
				ApexCodeCoverageAggregateDAO.insertApexCodeCoverage(apexCodeCoverageAggregate, connectionDB);
				
				
				if(result.isDone())
				{
					done = true;
				}else 
				{
					result = toolingConnection.queryMore(result.getQueryLocator());
				}
			}
		}
		
	}
}
