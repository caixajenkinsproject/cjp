package br.com.monitorSFDC.Service;

import java.io.IOException;
import java.util.Map;

import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.tooling.ToolingConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import br.com.monitorSFDC.Util.ConfigGetPropertyValues;
import br.com.monitorSFDC.Util.Constant;

public class LoginService 
{	
	static EnterpriseConnection connection;
	static ToolingConnection toolingConnection;
	
	public static ToolingConnection loginTooling() throws IOException, ConnectionException
	{
		ConfigGetPropertyValues properties = new ConfigGetPropertyValues();
		Map<String, String> mapUserInformation = properties.getPropValeus();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(mapUserInformation.get(Constant.KEY_USERNAME));
		config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORD));
		toolingConnection = com.sforce.soap.tooling.Connector.newConnection(config);
		return toolingConnection;
	}
	
	public static void logoutTooling(ToolingConnection toolingConnection) throws ConnectionException
	{
		toolingConnection.logout();
	}
	
	public static EnterpriseConnection login() throws ConnectionException, IOException
	{
		ConfigGetPropertyValues properties = new ConfigGetPropertyValues();
		Map<String, String> mapUserInformation = properties.getPropValeus();
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(mapUserInformation.get(Constant.KEY_USERNAME));
		config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORD));		
		connection = Connector.newConnection(config);
		return connection;
	}
	
	public static void logout(EnterpriseConnection connection) throws ConnectionException
	{
		connection.logout();
	}

}
