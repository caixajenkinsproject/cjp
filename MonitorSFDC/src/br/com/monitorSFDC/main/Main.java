package br.com.monitorSFDC.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.tooling.ToolingConnection;
import com.sforce.ws.ConnectionException;

import br.com.monitorSFDC.DAO.BD;
import br.com.monitorSFDC.Service.ApexCodeCoverageAggregateService;
import br.com.monitorSFDC.Service.LoginService;
import br.com.monitorSFDC.Service.SetupAuditTrailService;

public class Main 
{
	
	public static void main(String[] args) 
	{
		try
		{
			Connection connectionDB;
			EnterpriseConnection connection;
			ToolingConnection toolingConnection;
			String Option = JOptionPane.showInputDialog
					( "1 - SetupAuditTrail\n"
					+ "2 - Apex Code Coverage\n"
					+ "3 - Apex Class Name"
					+ "4 - Apex Class Test Name");
			
			switch (Option)
			{
				case "1":
					connection = LoginService.login();
					connectionDB = BD.getConnection();
					SetupAuditTrailService.consultAudit(connection, connectionDB);
					BD.fecharConexao();
					LoginService.logout(connection);
				break;
				
				case "2":
					toolingConnection = LoginService.loginTooling();
					connectionDB = BD.getConnection();
					ApexCodeCoverageAggregateService.consultApexCodeCoverage(toolingConnection, connectionDB);
					BD.fecharConexao();
					LoginService.logoutTooling(toolingConnection);
				break;
				
				case "3":
					
				break;
				
				default:
				break;
			}

		} catch (ConnectionException | IOException | SQLException e)
		{
			e.printStackTrace();
		}
	}
}
