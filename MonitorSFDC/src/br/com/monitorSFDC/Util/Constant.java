package br.com.monitorSFDC.Util;

public class Constant
{
	public static final String NAME_OF_CONFIG_FILE = "config.properties";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
}
