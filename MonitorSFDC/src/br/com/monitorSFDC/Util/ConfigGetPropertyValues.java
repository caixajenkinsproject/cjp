package br.com.monitorSFDC.Util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigGetPropertyValues
{
	Map<String, String> mapInformations = new HashMap<String, String>();
	InputStream inputStream;

	public Map<String, String> getPropValeus() throws IOException
	{
		Properties prop = new Properties();
		inputStream = getClass().getClassLoader().getResourceAsStream(Constant.NAME_OF_CONFIG_FILE);
		if (inputStream != null)
		{
			prop.load(inputStream);
		} else
		{
			throw new FileNotFoundException("property file '"+Constant.NAME_OF_CONFIG_FILE+"' not found in the classpath");
		}
		mapInformations.put(Constant.KEY_USERNAME, prop.getProperty(Constant.KEY_USERNAME));
		mapInformations.put(Constant.KEY_PASSWORD, prop.getProperty(Constant.KEY_PASSWORD));
		return mapInformations;

	}

}
