package br.com.carga.DAO;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.ws.ConnectionException;

public class CustomSettingsDAO
{
	public static QueryResult getOAuthInformation(EnterpriseConnection connection, String customSettingName) throws ConnectionException
	{
		String soqlQuery = "SELECT Id, Acess_Token__c, Last_Update_Date__c, Timeout__c FROM CS_OAuth__c WHERE Name = \'"+customSettingName+"\'";
		
		QueryResult result = connection.query(soqlQuery);
		
		if(result.getSize() > 0)
		{
			return result;
		}else
		{
			throw new ConnectionException("No records found.");
		}
	}
}
