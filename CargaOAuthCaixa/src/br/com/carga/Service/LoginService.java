package br.com.carga.Service;

import java.io.IOException;
import java.util.Map;

import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import br.com.carga.Util.Constant;

public class LoginService 
{	
	static EnterpriseConnection connection;
	
	public static EnterpriseConnection login(String ambiente) throws ConnectionException, IOException
	{
		br.com.carga.Util.ConfigGetPropertyValues properties = new br.com.carga.Util.ConfigGetPropertyValues();
		Map<String, String> mapUserInformation = properties.getPropValeus();
		ConnectorConfig config = new ConnectorConfig();
		if(ambiente.equalsIgnoreCase("Dev"))
		{
			config.setUsername(mapUserInformation.get(Constant.KEY_USERNAMEDEV));
			config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORDDEV));
		}else if(ambiente.equalsIgnoreCase("Qa"))
		{
			config.setUsername(mapUserInformation.get(Constant.KEY_USERNAMEQA));
			config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORDQA));
		}else if(ambiente.equalsIgnoreCase("Git1"))
		{
			config.setUsername(mapUserInformation.get(Constant.KEY_USERNAMEGIT1));
			config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORDGIT1));
		}else if(ambiente.equalsIgnoreCase("Git2"))
		{
			config.setUsername(mapUserInformation.get(Constant.KEY_USERNAMEGIT2));
			config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORDGIT2));
		}else if(ambiente.equalsIgnoreCase("Git3"))
		{
			config.setUsername(mapUserInformation.get(Constant.KEY_USERNAMEGIT3));
			config.setPassword(mapUserInformation.get(Constant.KEY_PASSWORDGIT3));
		}
		connection = Connector.newConnection(config);
		return connection;
	}
	
	public static void logout(EnterpriseConnection connection) throws ConnectionException
	{
		connection.logout();
	}

}
