package br.com.carga.Service;

import java.io.IOException;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.Error;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.CS_OAuth__c;
import com.sforce.soap.enterprise.sobject.SObject;
import com.sforce.ws.ConnectionException;

import br.com.carga.DAO.CustomSettingsDAO;

public class OAuthCustomSetttingService
{
	public static void consultOAuth(EnterpriseConnection connection) throws ConnectionException, IOException
	{
		EnterpriseConnection connectionDev = LoginService.login("Dev");
		EnterpriseConnection connectionGit1 = LoginService.login("Git1");
		EnterpriseConnection connectionGit2 = LoginService.login("Git2");
		EnterpriseConnection connectionGit3 = LoginService.login("Git3");
		
		CS_OAuth__c csOAuthQa = (CS_OAuth__c) CustomSettingsDAO.getOAuthInformation(connection, "OAuth").getRecords()[0];
		CS_OAuth__c csOAuthDev = (CS_OAuth__c) CustomSettingsDAO.getOAuthInformation(connectionDev, "OAuth").getRecords()[0];
		CS_OAuth__c csOAuthGit1 = (CS_OAuth__c) CustomSettingsDAO.getOAuthInformation(connectionGit1, "OAuth").getRecords()[0];
		CS_OAuth__c csOAuthGit2 = (CS_OAuth__c) CustomSettingsDAO.getOAuthInformation(connectionGit2, "OAuth").getRecords()[0];
		CS_OAuth__c csOAuthGit3 = (CS_OAuth__c) CustomSettingsDAO.getOAuthInformation(connectionGit3, "OAuth").getRecords()[0];
		
		csOAuthDev.setAcess_Token__c(csOAuthQa.getAcess_Token__c());
		csOAuthDev.setLast_Update_Date__c(csOAuthQa.getLast_Update_Date__c());
		
		csOAuthGit1.setAcess_Token__c(csOAuthQa.getAcess_Token__c());
		csOAuthGit1.setLast_Update_Date__c(csOAuthQa.getLast_Update_Date__c());
		
		csOAuthGit2.setAcess_Token__c(csOAuthQa.getAcess_Token__c());
		csOAuthGit2.setLast_Update_Date__c(csOAuthQa.getLast_Update_Date__c());
		
		csOAuthGit3.setAcess_Token__c(csOAuthQa.getAcess_Token__c());
		csOAuthGit3.setLast_Update_Date__c(csOAuthQa.getLast_Update_Date__c());
		
		SObject[] toUpdate = {csOAuthDev,csOAuthGit1,csOAuthGit2,csOAuthGit3};
		
		SaveResult[] saveResult = connectionDev.update(toUpdate);
		for (int i = 0; i < saveResult.length; i++)
		{
			if(saveResult[i].isSuccess())
			{
				System.out.println(i+". Successfully updated record - Id: " + saveResult[i].getId());
			}else
			{
				Error[] errors = saveResult[i].getErrors();
				for (int j = 0; j < errors.length; j++)
				{
					System.out.println("ERROR updating record: " + errors[j].getMessage());
				}
			}
		}
		System.out.println("Fim do Import\n\n");
		LoginService.logout(connectionDev);
	}
}
