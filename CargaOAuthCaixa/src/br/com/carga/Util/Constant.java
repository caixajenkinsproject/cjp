package br.com.carga.Util;

import com.sforce.soap.enterprise.EnterpriseConnection;

import br.com.carga.Service.LoginService;
import br.com.carga.Service.OAuthCustomSetttingService;

public class Constant
{
	public static final String NAME_OF_CONFIG_FILE = "config.properties";
	
	public static final String KEY_USERNAMEDEV = "usernameDev";
	public static final String KEY_PASSWORDDEV = "passwordDev";
	
	public static final String KEY_USERNAMEQA = "usernameQa";
	public static final String KEY_PASSWORDQA = "passwordQa";
	
	public static final String KEY_USERNAMEGIT1 = "usernameDev";
	public static final String KEY_PASSWORDGIT1 = "passwordDev";
	
	public static final String KEY_USERNAMEGIT2 = "usernameDev";
	public static final String KEY_PASSWORDGIT2 = "passwordDev";
	
	public static final String KEY_USERNAMEGIT3 = "usernameDev";
	public static final String KEY_PASSWORDGIT3 = "passwordDev";
	
}
