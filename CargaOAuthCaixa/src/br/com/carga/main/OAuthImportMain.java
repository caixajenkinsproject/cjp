package br.com.carga.main;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.ws.ConnectionException;

import br.com.carga.Service.LoginService;
import br.com.carga.Service.OAuthCustomSetttingService;

public class OAuthImportMain
{
	public static void main(String[] args)
	{
		
			Runnable runnable = () -> {
				try
				{
					EnterpriseConnection connectionQa = LoginService.login("Qa");
					OAuthCustomSetttingService.consultOAuth(connectionQa);
					LoginService.logout(connectionQa);
				} catch (ConnectionException | IOException  e)
				{
					e.printStackTrace();
				}
			};
			
			ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
			executorService.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.HOURS);
			
	}

}
